export interface IMap {
  gifts: IGift[];
  snowAreas: ISnowArea[];
  children: IChildren[];
}

export interface IGift {
  id: number;
  weight: number;
  volume: number;
}

export interface ISnowArea {
  r: number;
  x: number;
  y: number;
}

export interface IChildren {
  x: number;
  y: number;
}
