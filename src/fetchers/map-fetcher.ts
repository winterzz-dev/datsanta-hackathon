import { DataFetcher, IDataFetcherParams } from './data-fetcher';
import { IMap } from '../types';
import { TRoute } from '../libs';
import { TBag } from '../libs/bag-stack/types';

interface IMapFetcherParams extends IDataFetcherParams {
  mapId: string;
}

interface IResultBody {
  moves: TRoute;
  stackOfBags: TBag[];
}

interface IResult {
  success: boolean;
  error: string | null;
  roundId: string | null;
}

export class MapFetcher extends DataFetcher {
  private readonly MAP_ID: string;

  constructor({ mapId, ...baseParams }: IMapFetcherParams) {
    super(baseParams);
    this.MAP_ID = mapId;
  }

  public getMap() {
    return this.get<IMap>(`json/map/${this.MAP_ID}.json`);
  }

  public sendResult(body: IResultBody) {
    return this.post<IResultBody & { mapID: string }, IResult>('api/round', {
      ...body,
      mapID: this.MAP_ID,
    });
  }
}
