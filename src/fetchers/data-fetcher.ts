import axios, { AxiosInstance } from 'axios';

export interface IDataFetcherParams {
  basePath: string;
  authHeader: string;
  authToken: string;
}

export class DataFetcher {
  private readonly axios: AxiosInstance;

  constructor({ basePath, authHeader, authToken }: IDataFetcherParams) {
    this.axios = axios.create({
      baseURL: basePath,
    });
    this.axios.interceptors.request.use(config => ({
      ...config,
      headers: {
        ...config.headers,
        'Accept-Encoding': 'gzip,deflate,compress',
        [authHeader]: authToken,
      },
    }));
  }

  public async get<ResponseType>(url: string): Promise<ResponseType> {
    const response = await this.axios.get<ResponseType>(url);
    return response.data;
  }

  public async post<Body, ResponseType>(
    url: string,
    body: Body
  ): Promise<ResponseType> {
    const response = await this.axios.post<ResponseType>(url, body);
    return response.data;
  }
}
