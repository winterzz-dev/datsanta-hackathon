import { IChildren, ISnowArea } from '../../types';

export type TCoordinates = IChildren;
export type TSlowZone = ISnowArea;

export interface IRouteItem {
  x: number;
  y: number;
}

export type TRoute = IRouteItem[];
