import { TCoordinates } from './types';

/**
 * Класс ноды графа
 */
export class Node {
  public coordinates: TCoordinates;

  public isVisited = false;

  constructor(coordinates: TCoordinates) {
    this.coordinates = coordinates;
  }

  public markVisited(): void {
    this.isVisited = true;
  }
}
