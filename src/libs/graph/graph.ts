import { Node } from './node';
import { IRouteItem, TRoute, TSlowZone } from './types';
import { convertNodeToRouteItem, getPathLength } from './utils';

/**
 * Класс графа
 */
export class Graph {
  /**
   * Ноды графа
   * @private
   */
  private nodes: Node[];

  /**
   * Зоны замедления
   * @private
   */
  private readonly slowZones: TSlowZone[];

  /**
   * Коэффициент замедления в зоне
   * @private
   */
  private readonly SLOW_ZONE_EFFICIENCY: number;

  /**
   * Объект начала координат для возвращения в точку старта
   * @private
   */
  private readonly START_ZONE_ITEM: IRouteItem = {
    x: 0,
    y: 0,
  };

  constructor(nodes: Node[], slowZones: TSlowZone[], efficiency: number) {
    this.nodes = nodes;
    this.slowZones = slowZones;
    this.SLOW_ZONE_EFFICIENCY = efficiency;
  }

  /**
   * Метод для прохождения нескольких нод в графе
   * @param count
   */
  public visitNodes(count: number): TRoute {
    const path: TRoute = [];
    let nodesVisited = 1;
    let previousNode = this.getFarNode();
    path.push(convertNodeToRouteItem(previousNode));

    while (nodesVisited < count) {
      const nextNode = this.getNearestNode(previousNode);
      nextNode.markVisited();
      previousNode = nextNode;
      path.push(convertNodeToRouteItem(previousNode));
      nodesVisited += 1;
    }

    path.push(this.START_ZONE_ITEM);
    return path;
  }

  /**
   * Метод для поиска дальнейшей ноды от начала координат
   * @private
   */
  private getFarNode(): Node {
    const notVisitedNodes = this.nodes.filter(node => !node.isVisited);
    let maximumPathLength = 0;
    let farNode = notVisitedNodes[0];

    notVisitedNodes.forEach(node => {
      const pathLength = this.getPathLengthFromStart(node);
      if (pathLength > maximumPathLength) {
        maximumPathLength = pathLength;
        farNode = node;
      }
    });

    return farNode;
  }

  /**
   * Метод для поиска ближайшей ноды к началу координат или переданной точке
   * @private
   */
  private getNearestNode(from?: Node): Node {
    const notVisitedNodes = this.nodes.filter(node => !node.isVisited);
    let minimalPathLength = Number.MAX_VALUE;
    let nearestNode = notVisitedNodes[0];

    notVisitedNodes.forEach(node => {
      const pathLength = from
        ? this.getPathLengthWithSlowdown(from, node)
        : this.getPathLengthFromStart(node);
      if (pathLength < minimalPathLength) {
        minimalPathLength = pathLength;
        nearestNode = node;
      }
    });

    return nearestNode;
  }

  /**
   * Метод для поиска расстояния между двумя точками с учетом заснеженных зон
   * @param from - объект ноды начала пути
   * @param to - объект ноды конца пути
   * @private
   */
  private getPathLengthWithSlowdown(from: Node, to: Node) {
    const totalPathLength = getPathLength(from, to);
    let pathLengthOverSlowZones = 0;
    this.slowZones.forEach(zone => {
      const isInteractWithSlowZone =
        zone.y ===
        (zone.x * to.coordinates.y -
          zone.x * from.coordinates.y -
          from.coordinates.x * to.coordinates.y +
          from.coordinates.x * from.coordinates.y +
          to.coordinates.x -
          from.coordinates.x) /
          to.coordinates.x -
          from.coordinates.x;
      if (isInteractWithSlowZone && zone.r < totalPathLength) {
        pathLengthOverSlowZones += zone.r;
      }
    });

    return (
      totalPathLength -
      pathLengthOverSlowZones +
      pathLengthOverSlowZones * this.SLOW_ZONE_EFFICIENCY
    );
  }

  /**
   * Метод для поиска расстояния от начала координат до точки
   * @param to - объект ноды конца пути
   * @private
   */
  private getPathLengthFromStart(to: Node): number {
    const startNode = new Node({ x: 0, y: 0 });
    return this.getPathLengthWithSlowdown(startNode, to);
  }
}
