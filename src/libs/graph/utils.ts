import { Node } from './node';
import { IRouteItem } from './types';

/**
 * Метод для поиска расстояния между двумя точками
 * @param from - объект ноды начала пути
 * @param to - объект ноды конца пути
 */
export const getPathLength = (from: Node, to: Node) => {
  return Math.sqrt(
    (to.coordinates.x - from.coordinates.x) ** 2 +
      (to.coordinates.y - from.coordinates.y) ** 2
  );
};

/**
 * Метод преобразования ноды в объект пути
 * @param node
 */
export const convertNodeToRouteItem = (node: Node): IRouteItem => {
  return {
    x: node.coordinates.x,
    y: node.coordinates.y,
  };
};
