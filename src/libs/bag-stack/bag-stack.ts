import { IGift } from '../../types';
import { IStackGift, TBag } from './types';

export class BagStack {
  /**
   * Константа максимального веса мешка
   * @private
   */
  private readonly MAX_WEIGHT = 200;

  /**
   * Константа максимального объема мешка
   * @private
   */
  private readonly MAX_VOLUME = 100;

  private gifts: IStackGift[];

  constructor(gifts: IGift[]) {
    this.gifts = gifts.map(gift => ({
      ...gift,
      isPicked: false,
    }));
  }

  /**
   * Метод для наполнения мешков
   */
  public combineBags(): TBag[] {
    const bags: TBag[] = [];
    while (this.checkNotPickedGifts()) {
      const notPickedGifts = this.getNotPickedGifts();
      const pickedByWeight = this.tryToPickByParam(notPickedGifts, 'weight');
      const pickedByVolume = this.tryToPickByParam(notPickedGifts, 'volume');
      if (pickedByWeight.length >= pickedByVolume.length) {
        bags.push(pickedByWeight);
        this.gifts.forEach(gift => {
          if (pickedByWeight.includes(gift.id)) {
            gift.isPicked = true;
          }
        });
      } else {
        bags.push(pickedByVolume);
        this.gifts.forEach(gift => {
          if (pickedByVolume.includes(gift.id)) {
            gift.isPicked = true;
          }
        });
      }
    }
    return bags.reverse();
  }

  /**
   * Метод для набора мешка по параметру (метод будет пытаться наполнить мешок максимально в соответствии с параметрами)
   * @param gifts - набор невыбранных подарков
   * @param param - параметр подарка
   * @private
   */
  private tryToPickByParam(
    gifts: Array<IStackGift | IGift>,
    param: keyof IGift
  ): number[] {
    const ids: number[] = [];
    let totalWeight = 0;
    let totalVolume = 0;

    const sortedByWeight = gifts.sort((a, b) => a[param] - b[param]);
    sortedByWeight.forEach(gift => {
      if (
        totalWeight + gift.weight > this.MAX_WEIGHT ||
        totalVolume + gift.volume > this.MAX_VOLUME
      ) {
        return;
      }

      ids.push(gift.id);
      totalVolume += gift.volume;
      totalWeight += gift.weight;
    });

    return ids;
  }

  /**
   * Метод для проверки, остались ли еще не отсортированные подарки
   */
  private checkNotPickedGifts(): boolean {
    return this.gifts.filter(node => !node.isPicked).length > 0;
  }

  /**
   * Метод для получения невыбранных подарков
   * @private
   */
  private getNotPickedGifts(): IStackGift[] {
    return this.gifts.filter(node => !node.isPicked);
  }
}
