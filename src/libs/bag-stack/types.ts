import { IGift } from '../../types';

export interface IStackGift extends IGift {
  isPicked: boolean;
}

export type TBag = number[];
