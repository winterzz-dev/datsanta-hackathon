import 'dotenv/config';
import { MapFetcher } from './fetchers';
import {
  DrawService,
  FileSystemService,
  LoggerService,
  LogLevel,
} from './services';
import { IMAGE_SIZE, MAP_FILE_NAME, MAP_IMAGE_FILE_NAME } from './constants';
import { IChildren, IMap, ISnowArea } from './types';
import { BagStack, Graph, Node as GraphNode, TRoute } from './libs';

const { BASE_PATH, API_AUTH_HEADER, API_TOKEN, MAP_ID } = process.env;

const main = async (): Promise<void> => {
  const mapFetcher = new MapFetcher({
    basePath: BASE_PATH,
    authHeader: API_AUTH_HEADER,
    authToken: API_TOKEN,
    mapId: MAP_ID,
  });

  let map: IMap;

  /**
   * Загружаем карту с диска, если есть, иначе получаем через API
   */
  if (FileSystemService.checkExist(MAP_FILE_NAME)) {
    map = await FileSystemService.read<IMap>(MAP_FILE_NAME);
    LoggerService.log(LogLevel.INFO, 'карта загружена с диска');
  } else {
    map = await mapFetcher.getMap();
    await FileSystemService.write(MAP_FILE_NAME, map, true);
    LoggerService.log(
      LogLevel.INFO,
      'карта загружена через API и сохранена на диске'
    );
  }

  /**
   * Рисуем карту, если нет на диске
   */
  if (!FileSystemService.checkExist(MAP_IMAGE_FILE_NAME)) {
    const drawService = new DrawService<IChildren, ISnowArea>(
      IMAGE_SIZE,
      IMAGE_SIZE
    );
    drawService.init().drawDots(map.children).drawCircles(map.snowAreas);
    const buffer = drawService.export();
    await FileSystemService.write(MAP_IMAGE_FILE_NAME, buffer);
    LoggerService.log(LogLevel.INFO, 'изображение карты сохранено на диск');
  }

  const bagStack = new BagStack(map.gifts);
  const bags = bagStack.combineBags();
  LoggerService.log(LogLevel.INFO, 'мешки сгенерированы');

  const graph = new Graph(
    map.children.map(
      item =>
        new GraphNode({
          x: item.x,
          y: item.y,
        })
    ),
    map.snowAreas,
    7
  );

  const paths: TRoute[] = [];
  bags.reverse().forEach(bag => {
    paths.push(graph.visitNodes(bag.length));
  });
  LoggerService.log(LogLevel.INFO, 'путь построен');

  const result = await mapFetcher.sendResult({
    moves: paths.flat(),
    stackOfBags: bags,
  });

  LoggerService.log(
    LogLevel.INFO,
    `Результат запроса: ${result.success ? 'успешно' : 'ошибка'}, ошибки: ${
      result.error
    }, идентификатор: ${result.roundId}`
  );
};

void main();
