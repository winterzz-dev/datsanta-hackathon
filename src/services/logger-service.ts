export enum LogLevel {
  INFO = 'INFO',
}

export class LoggerService {
  public static log(level: LogLevel, message: string) {
    console.log(`[${level}]: ${message}`);
  }
}
