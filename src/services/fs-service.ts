import { existsSync } from 'fs';
import { writeFile, readFile } from 'fs/promises';
import { join } from 'path';

export class FileSystemService {
  /**
   * Утилита для получения пути файла
   * @param name
   * @private
   */
  private static getPath(name: string): string {
    return join(__dirname, '../', name);
  }

  /**
   * Метод для записи файла на диск
   * @param name - имя файла
   * @param data - контент файла
   * @param stringify - флаг указывающий, нужно ли преобразовать контент к строке
   */
  public static write(
    name: string,
    data: string | object,
    stringify = false
  ): Promise<void> {
    return writeFile(
      FileSystemService.getPath(name),
      stringify ? JSON.stringify(data) : (data as string)
    );
  }

  /**
   * Метод для чтения файла
   * @param name
   */
  public static async read<Data>(name: string): Promise<Data> {
    const content = await readFile(FileSystemService.getPath(name), 'utf-8');
    return JSON.parse(content) as Data;
  }

  public static checkExist(name: string): boolean {
    return existsSync(FileSystemService.getPath(name));
  }
}
