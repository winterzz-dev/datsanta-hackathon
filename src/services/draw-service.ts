import { Canvas, createCanvas } from 'canvas';

const BACKGROUND_COLOR = '#ffffff';
const DOTS_COLOR = '#000000';
const CIRCLE_STYLE = 'rgba(118,74,188,0.2)';
const DOT_SIZE = 10;

export class DrawService<
  Dot extends { x: number; y: number },
  CircleArea extends Dot & { r: number }
> {
  private layer: Canvas;

  private readonly WIDTH: number;

  private readonly HEIGHT: number;

  constructor(width: number, height: number) {
    this.layer = createCanvas(width, height);
    this.WIDTH = width;
    this.HEIGHT = height;
  }

  /**
   * Инициализация холста
   */
  public init(): DrawService<Dot, CircleArea> {
    const context = this.layer.getContext('2d');
    context.fillStyle = BACKGROUND_COLOR;
    context.fillRect(0, 0, this.WIDTH, this.HEIGHT);
    return this;
  }

  /**
   * Метод для рисования точек на холсте
   * @param dots
   */
  public drawDots(dots: Dot[]): DrawService<Dot, CircleArea> {
    const context = this.layer.getContext('2d');
    context.fillStyle = DOTS_COLOR;
    dots.forEach(dot => context.fillRect(dot.x, dot.y, DOT_SIZE, DOT_SIZE));
    return this;
  }

  /**
   * Метод для рисования окружностей на холсте
   * @param circles
   */
  public drawCircles(circles: CircleArea[]): DrawService<Dot, CircleArea> {
    const context = this.layer.getContext('2d');
    context.fillStyle = CIRCLE_STYLE;
    circles.forEach(circle => {
      context.beginPath();
      context.arc(circle.x, circle.y, circle.r, 0, Math.PI * 2, false);
      context.fill();
    });
    return this;
  }

  /**
   * Метод для экспорта изображения
   */
  public export() {
    return this.layer.toBuffer('image/jpeg', { quality: 1 });
  }
}
